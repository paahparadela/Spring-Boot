package com.itau.webspring.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.webspring.Carro;

public interface CarroRepository extends CrudRepository<Carro, Integer> {
	

}
