package com.itau.webspring;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.webspring.repository.CarroRepository;

@Controller
public class CarroController {
	ArrayList<Carro> carros = new ArrayList<>();
	
	@Autowired
	CarroRepository carroRepository;
	
	@RequestMapping(path="/carro", method=RequestMethod.GET)
	@ResponseBody
	public Carro getCarro() {
		Carro carro = new Carro();
		carro.setAno(2008);
		carro.setCor("laranja");
		carro.setMarca("Fiat");
		carro.setModelo("Ideia");
		carro.setPlaca("JJD-1212");
		
		return carro;
	}
	
	@RequestMapping(path="/carroPorAno", method=RequestMethod.GET)
	@ResponseBody
	public Carro getCarroPorAno(int ano) {
		for(Carro carro: this.carros) {
			if(carro.getAno() == ano) {
				return carro;
			}
		}
		return null;
	}
	
	//mais usual
	@RequestMapping(path="/carroByYear/{ano}", method=RequestMethod.GET)
	@ResponseBody
	public Carro getCarroByYear(@PathVariable(value="ano") int ano) {
		for(Carro carro: this.carros) {
			if(carro.getAno() == ano) {
				return carro;
			}
		}
		return null;
	}
	
	@RequestMapping(path="/carro", method=RequestMethod.POST)
	@ResponseBody
	public Carro inserirCarro(@RequestBody Carro carro) {
		this.carros.add(carro);
		return carroRepository.save(carro);
	}
	
	@RequestMapping(path="/carros", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Carro> getVariosCarros() {		
		return carroRepository.findAll();
	}

	
	
	@RequestMapping(path="/carro/lista", method=RequestMethod.GET)
	@ResponseBody
	public ArrayList<Carro> getListCarro() {		
		return this.carros;
	}
	
	
	@RequestMapping("/carroNovo")
	@ResponseBody
	public Carro getCarroNovo() {
		Carro carro = new Carro();
		carro.setAno(2018);
		carro.setCor("Vermelho");
		carro.setMarca("Chevrollet");
		carro.setModelo("Corsa Sedan");
		carro.setPlaca("PAE-4056");
		
		return carro;
	}
	
	@RequestMapping(path="/carroBom", method=RequestMethod.GET)
	public ResponseEntity<?> eBom(@RequestParam String modelo){
		return ResponseEntity.badRequest().build();
	}

}
